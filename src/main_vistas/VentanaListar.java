package main_vistas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author Luis
 */
public class VentanaListar extends JFrame implements ActionListener, TableModel{
    private Container mainConteiner;
    private JTable table;
    private JPanel panelTop;
    private JLabel lbRubro;
    private String titulos[] = {"nombre", "apellidos", "documentos", "Trabaja"};
    private Object datos[][] = {
        {"Juan", "Perez", new Integer(6999666), new Boolean(true)},
	{"Lucia", "Luca", new Integer(7888899), new Boolean(false)}};
    private JComboBox cbRubro;
    private String[] tipoRubro={"","Fiambreria","Limpieza","Despensa"};
    private JScrollPane sp;
    
    /*
    * Constructor de la Ventana Listar
    */
    public VentanaListar(String title){
        super(title);
        
        initComponent();
        
        this.setSize(1000,500);
        this.setLocation(100,100);
    }
    
    /*
    * Inicializacion de los componentes
    */
    public void initComponent(){
        panelTop = new JPanel();
        mainConteiner = this.getContentPane();
        cbRubro = new JComboBox(tipoRubro);
        lbRubro = new JLabel("Seleccione el rubro para listar");
        table = new JTable(datos, titulos);
        sp = new JScrollPane(table);
        
        mainConteiner.setLayout(new BorderLayout(8,6));
        this.getRootPane().setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, Color.GREEN));
        
        panelTop.setLayout(new FlowLayout(10, 10, 10));
        panelTop.add(lbRubro);
        panelTop.add(cbRubro);
        
        mainConteiner.add(panelTop, BorderLayout.NORTH);
        mainConteiner.add(sp, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    @Override
    public int getRowCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getColumnName(int columnIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
