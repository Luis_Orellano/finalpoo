/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main_vistas;

import dao.DAODespensa;
import dao.DAOFactura;
import dao.DAOFiambreria;
import dao.DAOLimpieza;
import dao.DAORenglones;
import entidades.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;


/**
 *
 * @author Reizhen
 */
public class VentanaPrincipal extends JFrame implements ActionListener{
    private Container mainContainer;
    private JPanel panelTop ,panelMiddleLeft, panelMiddleRight, panelBottom, panelGridLeft, panelGridRight;
    private JButton btAgregar,btSalir,btCerrarFactura, btInitFactura;
    private JLabel lbRubro, lbProducto, lbCantidad, lbTotal, lbFecha, lbHora, lbCliente;
    private JScrollPane spScroll;
    private JComboBox cbRubro, cbProducto;
    private JTextField txCantidad, txCliente, txTotalVenta;
    private JTextArea txAMuestraFactura;
    
    private String[] tipoRubro={"","Fiambreria","Limpieza","Despensa"};
    private DAOFiambreria dfia;   private DAODespensa ddes; private DAOLimpieza dlim; private DAORenglones dren; private DAOFactura dfac = new DAOFactura();
    private Fiambreria fi = new Fiambreria();    private Limpieza li = new Limpieza();  private Despensa de = new Despensa();  private Renglones re = new Renglones(); private Factura fa = new Factura();
    private List<Renglones> alr = new ArrayList<Renglones>(); private List<Fiambreria> afi = new ArrayList<Fiambreria>();  private List<Despensa> ade = new ArrayList<Despensa>(); private List<Limpieza> ali = new ArrayList<Limpieza>();

    private JButton btListarP;//btAgregarProduc, btEliminarProduc, btModifProduc;
    private JPanel panelProduc;
    private VentanaListar vCrud = null;
    
    public VentanaPrincipal(String title){
        super(title);
        
        initComponent();
        agregarActionListener();
        
        this.setSize(1000,500);
        this.setLocation(100,100);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public void agregarActionListener(){
        cbProducto.addActionListener(this);
        txCantidad.addActionListener(this);
        txCliente.addActionListener(this);
        btInitFactura.addActionListener(this);
        cbRubro.addActionListener(this);
        btAgregar.addActionListener(this);
        btCerrarFactura.addActionListener(this);
        btSalir.addActionListener(this);
//        btAgregarProduc.addActionListener(this);
        btListarP.addActionListener(this);
    }
    
    public void initComponent(){
        mainContainer = this.getContentPane();
        panelTop = new JPanel();
        panelMiddleLeft = new JPanel();
        panelMiddleRight = new JPanel();
        panelBottom = new JPanel();
        panelGridLeft = new JPanel();
        panelGridRight = new JPanel();
        panelProduc = new JPanel();
        
        txCliente = new JTextField(20);
        txCantidad = new JTextField("0");
        //txCantidad.setText("0");
        
        txTotalVenta = new JTextField();
        
        txAMuestraFactura = new JTextArea();
        
        spScroll = new JScrollPane(txAMuestraFactura);
        
        cbRubro = new JComboBox(tipoRubro);
        cbProducto = new JComboBox();
        
        //botones
        btAgregar = new JButton("Agregar");
        btSalir = new JButton("Salir");
        btCerrarFactura = new JButton("Cerrar Factura");
        btInitFactura = new JButton("Iniciar Factura");
//        btAgregarProduc = new JButton("Agregar Producto");
//        btEliminarProduc = new JButton("Eliminar Producto");
//        btModifProduc = new JButton("Modificar Producto");
        btListarP = new JButton("Listar Productos");

        //etiquetas
        lbCliente =  new JLabel("Nombre de Cliente");
        lbRubro = new JLabel("Rubro");
        lbProducto = new JLabel("Producto");
        lbCantidad = new JLabel("Cantidad");
        lbTotal = new JLabel("Total Venta");
        lbFecha = new JLabel(LocalDate.now().toString());
        lbHora = new JLabel(LocalTime.now().toString());
        
        mainContainer.setLayout(new BorderLayout(8,6));
        mainContainer.setBackground(Color.YELLOW);
        this.getRootPane().setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, Color.GREEN));
        
        //PanelTOP-------------------------------------------------------
        panelTop.setBorder(new LineBorder(Color.BLACK, 3));      
        panelTop.setLayout(new FlowLayout(10,10,10));
        panelTop.setBackground(Color.ORANGE);
        
        panelTop.add(lbCliente);
        panelTop.add(txCliente);
        panelTop.add(btInitFactura);
        panelTop.add(lbFecha);
        panelTop.add(lbHora);
        //---------------------------------------------------------------
        //PanelMIDDLE's--------------------------------------------------
        panelMiddleLeft.setBorder(new LineBorder(Color.BLACK, 3));
//        panelMiddleLeft.setLayout(new FlowLayout(4,4,4));
        panelMiddleLeft.setLayout(new GridLayout(3,3,20,20));
        panelMiddleLeft.setBackground(Color.RED);
//        btAgregarProduc.setBounds(2, 2, 2, 2);
//        panelMiddleLeft.add(btAgregarProduc);
        
//        panelProduc.setBorder(new LineBorder(Color.BLACK, 3));
        panelProduc.setLayout(new FlowLayout(1, 1, 2));
        panelProduc.setBackground(Color.RED);
        
        panelProduc.add(btListarP);
//        panelProduc.add(btAgregarProduc);
//        panelProduc.add(btEliminarProduc);
//        panelProduc.add(btModifProduc);
        
        panelGridLeft.setBorder(new LineBorder(Color.BLACK, 3));
        panelGridLeft.setLayout(new GridLayout(4,5,2,5));
        panelGridLeft.setBackground(Color.CYAN);
        
        panelGridLeft.add(lbRubro);
        panelGridLeft.add(cbRubro);
        panelGridLeft.add(lbProducto);
        panelGridLeft.add(cbProducto);
        panelGridLeft.add(lbCantidad);
        panelGridLeft.add(txCantidad);
        panelGridLeft.add(btAgregar);

        panelMiddleRight.setBorder(new LineBorder(Color.BLACK, 3));
        panelMiddleRight.setLayout(new FlowLayout(4,4,4));
        panelMiddleRight.setBackground(Color.RED);
        
        panelGridRight.setBorder(new LineBorder(Color.BLACK, 3));
        panelGridRight.setLayout(new GridLayout(2,5,1,5));
        panelGridRight.setBackground(Color.CYAN);
        
        panelGridRight.add(lbTotal);
        panelGridRight.add(txTotalVenta);
        txTotalVenta.setEditable(false);
        txTotalVenta.setOpaque(true);
        
        txAMuestraFactura.setBounds(new Rectangle(30,30,100,200));
        txAMuestraFactura.setLineWrap(true);
        txAMuestraFactura.setWrapStyleWord(true);
        
        txAMuestraFactura.setEditable(false);
        txAMuestraFactura.setOpaque(true);
        txAMuestraFactura.setBorder(new LineBorder(Color.BLACK, 3));
        
        panelMiddleRight.add(panelGridRight);
        panelMiddleLeft.add(panelGridLeft);
        panelMiddleLeft.add(panelProduc);
        
        //---------------------------------------------------------------
        //PanelBOTTOM----------------------------------------------------
        panelBottom.setBorder(new LineBorder(Color.BLACK, 3));
        panelBottom.setLayout(new FlowLayout(2));
        panelBottom.setBackground(Color.MAGENTA);
        panelBottom.add(btSalir);
        panelBottom.add(btCerrarFactura);
        //---------------------------------------------------------------
        
        mainContainer.add(panelTop, BorderLayout.NORTH);
        mainContainer.add(panelMiddleLeft, BorderLayout.WEST);
        mainContainer.add(spScroll,BorderLayout.CENTER);
        mainContainer.add(panelMiddleRight, BorderLayout.EAST);
        mainContainer.add(panelBottom, BorderLayout.SOUTH);
        
        
        desabilitarBotones();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        addFactura(e);
        MuestraProductos(e);
        addRenglon(e);
        closeFactura(e);
        Salir(e);
        
        btnAgrProd(e);
    }
    
    public void Salir(ActionEvent e){
        if (e.getSource()==btSalir) {
            setTitle("Saldrá en 3 segundos");
            try{
            Thread.sleep(3000);
            System.exit(0);
            } catch(InterruptedException ex) {
                System.exit(0);
            }
        }
    }
    
    public void addFactura(ActionEvent e){
        
        if(e.getSource()==btInitFactura){
            String cliente = String.valueOf((txCliente.getText()));
            if(cliente.equals("")){
                JOptionPane.showMessageDialog(null, "Ingrese un usuario");
            }else{
                habilitarBotones();
                Timestamp tmp = Timestamp.valueOf(lbFecha.getText() + " " + lbHora.getText());
                alr.clear();
                fa = new Factura(tmp,cliente);
                fa.setTotalVenta(0);
                dfac.cInsertar(fa);
                fa = new Factura(dfac.getFtm().getId(),tmp,cliente);
                txAMuestraFactura.setText("");
                txTotalVenta.setText("");
                lbFecha.setText(LocalDate.now().toString());
                lbHora.setText(LocalTime.now().toString());
            }
            
            
        }
    }
    
    public void MuestraProductos(ActionEvent e){
        if(e.getSource()==this.cbRubro){
            cbProducto.removeAllItems();
            String st=(String)cbRubro.getSelectedItem(); 
            if("Fiambreria".equals(st)){
                dfia = new DAOFiambreria();
                afi = dfia.rListar();
                for (Fiambreria f : afi){
                    cbProducto.addItem(f);
                    
                } 
            }
            if("Despensa".equals(st)){ 
                ddes = new DAODespensa();   
                List<Despensa> ds = ddes.rListar(); 
                for (Despensa d : ds){ 
                    cbProducto.addItem(d);
                    de=d;
                }
            }
            if("Limpieza".equals(st)){
                dlim = new DAOLimpieza();   
                List<Limpieza> ls = dlim.rListar(); 
                for (Limpieza l: ls){ 
                    cbProducto.addItem(l);
                    li=l;
                }
            }
        }
    }
    
    public void addRenglon(ActionEvent e){
        dren = new DAORenglones();
        String cliente = String.valueOf((txCliente.getText()));
        
        if(e.getSource()==this.btAgregar){
            if(cliente.equals("")){
                JOptionPane.showMessageDialog(null, "Ingrese un usuario");
            }else{
                float cant = 0;
                if(txCantidad.getText().equals("")){
                    cant = 0;
                }else{
                    try{
                        cant = Float.valueOf((txCantidad.getText()));
                    }catch(NumberFormatException ex){
                        JOptionPane.showMessageDialog(null, "Ingrese un numero entre el 1 y el 100 en la cantidad");
                    }
                }
                
                int num_reg = alr.size() + 1;

                String st=(String)cbRubro.getSelectedItem();
                
                if("".equals(st) || (cant<=0) || (cant>=100)){
                    JOptionPane.showMessageDialog(null, "Ingrese un producto");
                }else{
                    if("Fiambreria".equals(st)){

                    fi = (Fiambreria) cbProducto.getSelectedItem();

                    re = new Renglones(num_reg,fi,cant,fi.calcularPconImpuesto());

                    String temp = txAMuestraFactura.getText();

                    txAMuestraFactura.setText(temp+"\n"+re.toString()+"\t| "+re.calcularTotalRenglon());

                    re = new Renglones(fa.getId(),num_reg,fi.getId(),cant,fi.calcularPconImpuesto());
                    dren.cInsertar(re);
                    alr.add(re);

                }
                if("Limpieza".equals(st)){
                    li = (Limpieza) cbProducto.getSelectedItem();
                    re = new Renglones(num_reg,li,cant,li.calcularPconImpuesto());
                    String temp = txAMuestraFactura.getText();

                    txAMuestraFactura.setText(temp+"\n"+re.toString()+"\t| "+re.calcularTotalRenglon());
                    re = new Renglones(fa.getId(),num_reg,li.getId(),cant,li.calcularPconImpuesto());
                    dren.cInsertar(re);
                    alr.add(re);
                }
                if("Despensa".equals(st)){
                    de = (Despensa) cbProducto.getSelectedItem();
                    re = new Renglones(num_reg,de,cant,de.calcularPconImpuesto());
                    String temp = txAMuestraFactura.getText();

                    txAMuestraFactura.setText(temp+"\n"+re.toString()+"\t| "+re.calcularTotalRenglon());
                    re = new Renglones(fa.getId(),num_reg,de.getId(),cant,de.calcularPconImpuesto());
                    dren.cInsertar(re);
                    alr.add(re);
                }
                    fa.setRenglones(alr);
                
                }
            } 
        }
    }

    public void closeFactura(ActionEvent e){
        dfac = new DAOFactura();
        if(e.getSource()==btCerrarFactura ){
            if("".equals(txAMuestraFactura.getText())){
                JOptionPane.showMessageDialog(null, "No puede cerrar la factura");
            }
            else{
                fa = new Factura(fa.getId(),fa.calcularTotal(alr));
            
                dfac.uActualizarTotal(fa);

                txTotalVenta.setText(fa.toString());
                JOptionPane.showMessageDialog(null, "El Total de la compra es: "+fa.toString());
                alr.clear();
                txCliente.setText("");
                lbFecha.setText(LocalDate.now().toString());
                lbHora.setText(LocalTime.now().toString());
                txCantidad.setText("0");
            }
        }
    }
    
    public void btnAgrProd(ActionEvent e){
        if(e.getSource()==this.btListarP){
            vCrud = new VentanaListar("Lista de Productos");
            vCrud.setVisible(true);
        }
    }
    
    public void desabilitarBotones(){
        btAgregar.setEnabled(false);
        btCerrarFactura.setEnabled(false);
        txCantidad.setEnabled(false);
        cbRubro.setEnabled(false);
        cbProducto.setEnabled(false);
    }
    
    public void habilitarBotones(){
        btAgregar.setEnabled(true);
        btCerrarFactura.setEnabled(true);
        txCantidad.setEnabled(true);
        cbRubro.setEnabled(true);
        cbProducto.setEnabled(true);
    }
    
}




