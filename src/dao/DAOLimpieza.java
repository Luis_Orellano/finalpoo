/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Limpieza;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Reizhen
 */
public class DAOLimpieza extends Conexion{
    Limpieza l = null;
    List<Limpieza> ll = null;
    
    public List<Limpieza> rListar(){
        ll = new ArrayList();
        try {
            conectar();           
            stmt = cn.createStatement();
            String sql = "SELECT * FROM productos WHERE rubro='Limpieza';";
            rs = stmt.executeQuery(sql);
            while(rs.next()) {
                if(rs.getString("rubro").equals("Limpieza")){
                    l = new Limpieza(rs.getInt("id_producto"),rs.getString("nombre_producto"),rs.getFloat("precio_unitario"),rs.getFloat("impuesto"),rs.getDate("fecha_vto").toLocalDate(),rs.getBoolean("toxico"));
                    ll.add(l);
                }
            }           
        } catch (SQLException ex) {
            Logger.getLogger(DAOFiambreria.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ll;
    }
}
