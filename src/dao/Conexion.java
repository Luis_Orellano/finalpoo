/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Reizhen
 */
public class Conexion {
    protected Connection cn = null;
    protected Statement stmt = null;
    protected ResultSet rs = null;
    
    public void conectar(){
        //libreria de mysql
        String driver ="org.mariadb.jdbc.Driver";
        //nombre de la base de dato
        String database ="tp_parcial_poo";
        //Host
        String hostname ="localhost";
        //Puerto
        String port ="3306";
        // Ruta de nuestra base de datos (desactivamos el uso de SSL con "?useSSL=false")
        String url ="jdbc:mysql://" + hostname + ":" + port + "/" + database + "?useSSL=false";
        //nombre usuario
        String username ="root";
        //clave usuario
        String password ="";
        
        try{
            Class.forName(driver);
            cn = DriverManager.getConnection(url, username, password);
            //System.out.println("Esta conectado");
        }catch (ClassNotFoundException | SQLException e){
            e.printStackTrace();
        }
    }
    
    public void desconectar() throws SQLException{
        
        if(rs!=null && !rs.isClosed()){
            cn.close();
        }
        if(stmt!=null && !stmt.isClosed()){
            cn.close();
        }
        if(cn!=null && !cn.isClosed()){
            cn.close();
        }
    }
    
    
}
