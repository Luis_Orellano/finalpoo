/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Fiambreria;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Reizhen
 */
public class DAOFiambreria extends Conexion{
    Fiambreria f = null;
    List<Fiambreria> lf = null;
    
    public List<Fiambreria> rListar(){
        lf = new ArrayList();
        try {
            conectar();
            
            stmt = cn.createStatement();
            String sql = "SELECT * FROM productos WHERE rubro='Fiambreria';";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                if(rs.getString("rubro").equals("Fiambreria")){
                    f = new Fiambreria(rs.getInt("id_producto"),rs.getString("nombre_producto"),rs.getFloat("precio_unitario"),rs.getFloat("impuesto"),rs.getDate("fecha_vto").toLocalDate(),rs.getString("unidad_medida"),rs.getBoolean("refrigeracion"));
                    lf.add(f);
                }
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DAOFiambreria.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lf;
    }
}
