/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import entidades.Renglones;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Reizhen
 */
public class DAORenglones extends Conexion{

    
    public void cInsertar(Renglones r){
        int idf= r.getId(),idp=r.getProducto().getId();
        try {
            conectar();
            
            stmt = cn.createStatement();
            String sql = "INSERT INTO renglones_factura(id_factura_f,num_renglon ,id_producto_f ,cantidad ,p_unitario_imp) VALUES ("+idf+","+r.getNumRenglones()+","+idp+","+r.getCantidad()+","+r.getPrecioUnitarioImp()+");";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                r = new Renglones(rs.getInt("id_factura_f"),rs.getInt("num_renglon"),rs.getInt("id_producto_f"),rs.getFloat("cantidad"),rs.getFloat("p_unitario_imp"));
            }
            desconectar();
        } catch (SQLException ex) {
            Logger.getLogger(DAOFiambreria.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
}
