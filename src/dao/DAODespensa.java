/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Despensa;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Reizhen
 */
public class DAODespensa extends Conexion{
    Despensa d = null;
    List<Despensa> ld = null;
    
    public List<Despensa> rListar(){
        ld = new ArrayList();
        try {
            conectar();
            stmt = cn.createStatement();
            String sql = "SELECT * FROM productos WHERE rubro='Despensa';";
            rs = stmt.executeQuery(sql);
            while(rs.next()){
                if(rs.getString("rubro").equals("Despensa")){
                    d = new Despensa(rs.getInt("id_producto"),rs.getString("nombre_producto"),rs.getFloat("precio_unitario"),rs.getFloat("impuesto"),rs.getDate("fecha_vto").toLocalDate(),rs.getString("presentacion"));
                    ld.add(d);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAODespensa.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ld;
    }
}
