/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Factura;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Reizhen
 */
public class DAOFactura extends Conexion{
    
    protected Factura ftm;

    public Factura getFtm() {
        return ftm;
    }

    public void setFtm(Factura ftm) {
        this.ftm = ftm;
    }
    
    public void cInsertar(Factura f){
        try {
            conectar();
            
            stmt = cn.createStatement();
            String sql = "INSERT INTO facturas(fecha_factura ,cliente ,total_venta) VALUES ('"+f.getFech_hora()+"','"+f.getCliente()+"',"+f.getTotalVenta()+");";
            rs = stmt.executeQuery(sql);
            sql = "SELECT LAST_INSERT_ID() as id_factura;";
            rs = stmt.executeQuery(sql);
            if (rs.next()){
                f.setId(rs.getInt("id_factura")); 
            }
            while (rs.next()) {
                f = new Factura(rs.getTimestamp("fecha_factura"),rs.getString("cliente"),rs.getFloat("total_venta"));
            }
            
            desconectar();
        } catch (SQLException ex) {
            Logger.getLogger(DAOFiambreria.class.getName()).log(Level.SEVERE, null, ex);
        }
        ftm = f;
        ftm.setId(f.getId());
    }
    
    public void uActualizarTotal(Factura f){
        try {
            conectar();
            
            stmt = cn.createStatement();
            String sql = "UPDATE facturas SET total_venta = "+f.getTotalVenta()+" WHERE facturas.id_factura ="+ f.getId()+";";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                f = new Factura(rs.getInt("id_factura"),rs.getFloat("total_venta"));
            }
            
            desconectar();
        } catch (SQLException ex) {
            Logger.getLogger(DAOFiambreria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
