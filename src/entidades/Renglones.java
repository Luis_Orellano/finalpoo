package entidades;

import java.util.Objects;

/**
 * @author Luis, Fernando, Tito
 */
public class Renglones extends Factura{
    protected int numRenglones;
    protected Producto pr = new Producto() {
        @Override
        public boolean hayStock() {
            throw new UnsupportedOperationException("Nada por ahora");
        }
    };

protected float cantidad;
protected float pUnit_imp;

public Renglones(int numRenglones, Producto pr, float cantidad, float pUnit_imp) {
    this.numRenglones = numRenglones;
    this.pr = pr;
    this.cantidad = cantidad;
    this.pUnit_imp = pUnit_imp;
}
    
    
    
    public Renglones(int id_fac, int numRenglones, int idp, float cantidad, float pUnit_imp) {
        super(id_fac);
        this.numRenglones = numRenglones;
        this.pr.setId(idp);
        this.cantidad = cantidad;
        this.pUnit_imp = pUnit_imp;
    }
    
    public Renglones(int id_fac, int numRenglones, Producto p, float cantidad, float pUnit_imp) {
        super(id_fac);
        this.numRenglones = numRenglones;
        pr = p;
        this.cantidad = cantidad;
        this.pUnit_imp = pUnit_imp;
    }
    
    public Renglones(int numRenglones, int id, float cantidad, float pUnit_imp) {
        this.numRenglones = numRenglones;
        this.pr.id = id;
        this.cantidad = cantidad;
        this.pUnit_imp = pUnit_imp;
    }
    
    public Renglones(Producto pr, float cantidad, float pUnit_imp) {
        this.pr = pr;
        this.cantidad = cantidad;
        this.pUnit_imp = pUnit_imp;
    }

    public Renglones(){}

    
    public float calcularTotalRenglon(){
        float totalR = pUnit_imp*cantidad;
        return totalR;
    }
    
    public int getNumRenglones() {
        return numRenglones;
    }

    public void setNumRenglones(int numRenglones) {
        this.numRenglones = numRenglones;
    }
    
    
    public Producto getProducto() {
        return pr;
    }

    public void setProducto(Producto producto) {
        this.pr = producto;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecioUnitarioImp() {
        return pUnit_imp;
    }

    public void setPrecioUnitarioImp(float precioUnitario) {
        this.pUnit_imp = precioUnitario;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Renglones other = (Renglones) obj;
        if (this.numRenglones != other.numRenglones) {
            return false;
        }
        if (Float.floatToIntBits(this.cantidad) != Float.floatToIntBits(other.cantidad)) {
            return false;
        }
        if (Float.floatToIntBits(this.pUnit_imp) != Float.floatToIntBits(other.pUnit_imp)) {
            return false;
        }
        if (!Objects.equals(this.pr, other.pr)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "#" + numRenglones + " | " +cantidad +" | " + pr + " | Precio Unitario: " + pUnit_imp;
    }
   
}
