package entidades;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;


/**
 * @author Luis, Fernando, Tito
 */
public class Factura {
    protected int id_fac;
    protected String cliente;
    protected float totalVenta = 0;
    protected List<Renglones> renglones;
    protected Timestamp fech_hora;
    
    
    public Factura() {}
     
    public Factura(Timestamp fecha, String cliente, float totalVenta, List<Renglones> renglones) {
        this.fech_hora = fecha;
        this.cliente = cliente;
        this.totalVenta = totalVenta;
        this.renglones = renglones;
    }

    public Factura(int id, Timestamp fecha, String cliente, float totalVenta, List<Renglones> renglones) {
        this.id_fac = id;
        this.fech_hora = fecha;
        this.cliente = cliente;
        this.totalVenta = totalVenta;
        this.renglones = renglones;
    }

    public Factura(Timestamp fecha, String cliente, float totalVenta ) {
        this.fech_hora = fecha;
        this.cliente = cliente;
        this.totalVenta = totalVenta;
    }

    public Factura(Timestamp fecha, String cliente) {
        this.fech_hora = fecha;
        this.cliente = cliente;
    }
    public Factura(int id,Timestamp fecha, String cliente) {
        this.id_fac = id;
        this.fech_hora = fecha;
        this.cliente = cliente;
    }
    
    public Factura(int id,float totalVenta ) {
        this.id_fac = id;
        this.totalVenta = totalVenta;
    }
    public Factura(int id){
        this.id_fac = id;
    }
    
    public float calcularTotal(List<Renglones> rs){
        renglones = rs;
        for(Renglones r : renglones){
            
            totalVenta += r.calcularTotalRenglon();
        }
        return totalVenta;
    }

    public int getId() {
        return id_fac;
    }

    public void setId(int id) {
        this.id_fac = id;
    }

    public Timestamp getFech_hora() {
        return fech_hora;
    }

    public void setFech_hora(Timestamp fech_hora) {
        this.fech_hora = fech_hora;
    }
   
    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public float getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(float totalVenta) {
        this.totalVenta = totalVenta;
    }

    public List<Renglones> getRenglones() {
        return renglones;
    }

    public void setRenglones(List<Renglones> renglones) {
        this.renglones = renglones;
    }
    
    
    public void mostrarFactura(){
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.id_fac;
        hash = 37 * hash + Objects.hashCode(this.cliente);
        hash = 37 * hash + Float.floatToIntBits(this.totalVenta);
        hash = 37 * hash + Objects.hashCode(this.renglones);
        hash = 37 * hash + Objects.hashCode(this.fech_hora);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Factura other = (Factura) obj;
        if (this.id_fac != other.id_fac) {
            return false;
        }
        if (Float.floatToIntBits(this.totalVenta) != Float.floatToIntBits(other.totalVenta)) {
            return false;
        }
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        if (!Objects.equals(this.renglones, other.renglones)) {
            return false;
        }
        if (!Objects.equals(this.fech_hora, other.fech_hora)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "" + totalVenta;
    }

    
    
    
}
