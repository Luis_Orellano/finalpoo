package entidades;

import java.time.LocalDate;

/**
 * @author Luis, Fernando, Tito
 */
public class Fiambreria extends Producto{
    protected String unidadMedida;
    protected boolean refrigeracion;
    protected Renglones renglones;

    public Fiambreria(int id, String nombre, float precioUnitario, float impuesto, LocalDate fechVto, String uniMed, boolean refri) {
        super(id, nombre, precioUnitario, impuesto, fechVto);
        this.unidadMedida = uniMed;
        this.refrigeracion = refri;
    }

     public Fiambreria(String nombre, float precioUnitario, float impuesto, LocalDate fechVto, String uniMed, boolean refri) {
        super( nombre, precioUnitario, impuesto, fechVto);
        this.unidadMedida = uniMed;
        this.refrigeracion = refri;
    }

    public Fiambreria(int id, String nombre, float precioUnitario,String unidadMedida, float impuesto, LocalDate caducable) {
        super(id, nombre, precioUnitario, impuesto, caducable);
        this.unidadMedida = unidadMedida;
    } 
    
    public Fiambreria(int id){
        this.id=id;
    }
    
     public Fiambreria(String nombre, float precioUnitario, float impuesto, LocalDate fechVto, float stock, String uniMed, boolean refri) {
        super( nombre, precioUnitario, impuesto, fechVto, stock);
        this.unidadMedida = uniMed;
        this.refrigeracion = refri;
    }
     
    public Fiambreria() {}
    
    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public boolean isRefrigeracion() {
        return refrigeracion;
    }

    public void setRefrigeracion(boolean refrigeracion) {
        this.refrigeracion = refrigeracion;
    }

    @Override
    public boolean hayStock() {
        if(renglones.cantidad>stock){
            System.out.println("No hay stock, Stock= "+this.stock);
            return false;
        }else{
            System.out.println("Si hay stock");
            return true;
        }
    }
    
    @Override
    public String toString(){
        return getNombre() /*+this.id + this.precioUnitario + this.unidadMedida + this.impuesto + this.isCaducable()*/;
    }
}
