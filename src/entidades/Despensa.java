package entidades;

import java.time.LocalDate;

/**
 * @author Luis, Fedrnando, Tito
 */

public class Despensa extends Producto{
    protected String presentacion; //caja, bolsa, pack de 10, etc;
    protected Renglones renglones;

    public Despensa(int id, String nombre, float precioUnitario, float impuesto, LocalDate fechaVencimiento, String presentacion) {
        super(id, nombre, precioUnitario, impuesto, fechaVencimiento);
        this.presentacion = presentacion;
    }
    
    public Despensa(String nombre, float precioUnitario, float impuesto, LocalDate fechaVencimiento) {
        super(nombre, precioUnitario, impuesto, fechaVencimiento);
    }

    public Despensa(String nombre, float precioUnitario, float impuesto, LocalDate fechaVencimiento, float stock, String presentacion) {
        super(nombre, precioUnitario, impuesto, fechaVencimiento, stock);
        this.presentacion = presentacion;
    }
        
    public Despensa() {}


    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    @Override
    public boolean hayStock() {
        if(renglones.cantidad>stock){
            System.out.println("No hay stock, Stock= "+this.stock);
            return false;
        }else{
            System.out.println("Si hay stock");
            return true;
        }
    }

    @Override
    public String toString() {
        return this.nombre;
    }
    
     
}
