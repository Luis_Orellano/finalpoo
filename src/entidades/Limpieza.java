package entidades;

import java.time.LocalDate;

/**
 * @author Luis, Fedrnando, Tito
 */
public class Limpieza extends Producto{
    protected boolean toxico;
    protected Renglones renglones;

    public Limpieza(String nombre, float precioUnitario, float impuesto, LocalDate caducable) {
        super(nombre, precioUnitario, impuesto, caducable);
    }

    public Limpieza(int id,String nombre, float precioUnitario, float impuesto, LocalDate caducable, boolean toxico) {
        super(id,nombre, precioUnitario, impuesto, caducable);
        this.toxico = toxico;
    }
    
    public Limpieza(String nombre, float precioUnitario, float impuesto, LocalDate caducable, float stock, boolean toxico) {
        super(nombre, precioUnitario, impuesto, caducable, stock);
        this.toxico = toxico;
    }
    
    public Limpieza() {}

    public boolean isToxico() {
        return toxico;
    }

    public void setToxico(boolean toxico) {
        this.toxico = toxico;
    }

    @Override
    public boolean hayStock() {
        if(renglones.cantidad>stock){
            System.out.println("No hay stock, Stock= "+this.stock);
            return false;
        }else{
            System.out.println("Si hay stock");
            return true;
        }
    }

    @Override
    public String toString() {
        return this.nombre;
    }
    
    
}
