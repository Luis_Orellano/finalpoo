package entidades;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Luis,Fernando, Tito
 */
public abstract class Producto {
    protected int id;
    protected String nombre;
    protected float precioUnitario;
    protected float impuesto;
    protected LocalDate fechaVencimiento;
    protected float stock;

    public Producto(String nombre, float precioUnitario, float impuesto, LocalDate caducable) {
        this.nombre = nombre;
        this.precioUnitario = precioUnitario;
        this.impuesto = impuesto;
        this.fechaVencimiento = caducable;
    }

    public Producto(int id, String nombre, float precioUnitario, float impuesto, LocalDate fechaVencimiento) {
        this.id = id;
        this.nombre = nombre;
        this.precioUnitario = precioUnitario;
        this.impuesto = impuesto;
        this.fechaVencimiento = fechaVencimiento;
    }
    
   public Producto(String nombre, float precioUnitario, float impuesto, LocalDate fechaVencimiento, float stock) {
        this.nombre = nombre;
        this.precioUnitario = precioUnitario;
        this.impuesto = impuesto;
        this.fechaVencimiento = fechaVencimiento;
        this.stock = stock;
    } 
    
    public Producto(){}

    public float calcularPconImpuesto(){
        float pUnit_imp = this.precioUnitario * (1 + this.impuesto);
        return pUnit_imp;
    }
    
    public abstract boolean hayStock();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public float getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(float impuesto) {
        this.impuesto = impuesto;
    }

    public LocalDate isCaducable() {
        return fechaVencimiento;
    }

    public void setCaducable(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public float getStock() {
        return stock;
    }

    public void setStock(float stock) {
        this.stock = stock;
    }
    
    public LocalDate caduco(){
        LocalDate c = LocalDate.now();
        if(fechaVencimiento.getDayOfMonth()<c.getDayOfMonth() || fechaVencimiento.getDayOfYear()<c.getDayOfYear()){
            System.out.println("El producto caduco");
            
        }else{
            System.out.println("El producto no caduco");
        }
           
        return fechaVencimiento;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Producto other = (Producto) obj;
        if (Float.floatToIntBits(this.precioUnitario) != Float.floatToIntBits(other.precioUnitario)) {
            return false;
        }
        if (Float.floatToIntBits(this.impuesto) != Float.floatToIntBits(other.impuesto)) {
            return false;
        }
        if (this.fechaVencimiento != other.fechaVencimiento) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Producto{" + "nombre=" + nombre + ", precioUnitario=" + precioUnitario + ", impuesto=" + impuesto + ", caducable=" + fechaVencimiento + '}' + "\n";
    }
    
    
    
    
}
