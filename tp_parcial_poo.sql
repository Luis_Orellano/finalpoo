-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-11-2019 a las 01:20:52
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5
create database tp_parcial_poo;
use tp_parcial_poo;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tp_parcial_poo`
--
-- drop database tp_parcial_poo;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `id_factura` int(11) NOT NULL,
  `fecha_factura` date DEFAULT NULL,
  `cliente` varchar(30) DEFAULT NULL,
  `total_venta` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `rubro` enum('Despensa','Fiambreria','Limpieza') NOT NULL,
  `nombre_producto` varchar(30) NOT NULL,
  `precio_unitario` float NOT NULL,
  `impuesto` float DEFAULT NULL,
  `fecha_vto` datetime DEFAULT NULL,
  `toxico` tinyint(1) DEFAULT NULL,
  `presentacion` varchar(30) DEFAULT NULL,
  `unidad_medida` varchar(10) DEFAULT NULL,
  `refrigeracion` tinyint(1) DEFAULT NULL,
  `stock` float NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `rubro`, `nombre_producto`, `precio_unitario`, `impuesto`, `fecha_vto`, `toxico`, `presentacion`, `unidad_medida`, `refrigeracion`, `stock`, `visible`) VALUES
(1, 'Despensa', 'Yerba', 125, 0.21, '2020-11-30 00:00:00', NULL, 'Por Kilo', NULL, NULL, 50, 1),
(2, 'Despensa', 'Azucar', 49.99, 0.21, '2020-11-14 00:00:00', NULL, 'Por Kilo', NULL, NULL, 60, 1),
(3, 'Fiambreria', 'Queso Cremoso', 240, 0.21, '2020-11-30 00:00:00', NULL, NULL, '1kg', 1, 40, 1),
(4, 'Fiambreria', 'Aceituna', 500, 0.21, '2020-11-21 00:00:00', NULL, NULL, '1kg', 1, 100, 1),
(5, 'Limpieza', 'Lavandina', 20, 0.21, '2020-11-14 00:00:00', 1, NULL, NULL, NULL, 75, 1),
(6, 'Limpieza', 'Detargente', 40, 0.21, '2020-11-28 00:00:00', 1, NULL, NULL, NULL, 65, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `renglones_factura`
--

CREATE TABLE `renglones_factura` (
  `id_factura_f` int(11) NOT NULL,
  `num_renglon` int(11) NOT NULL,
  `id_producto_f` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `p_unitario_imp` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`id_factura`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `renglones_factura`
--
ALTER TABLE `renglones_factura`
  ADD PRIMARY KEY (`id_factura_f`,`num_renglon`),
  ADD KEY `fk_id_pro` (`id_producto_f`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `id_factura` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `renglones_factura`
--
ALTER TABLE `renglones_factura`
  ADD CONSTRAINT `fk_id_fac` FOREIGN KEY (`id_factura_f`) REFERENCES `facturas` (`id_factura`),
  ADD CONSTRAINT `fk_id_pro` FOREIGN KEY (`id_producto_f`) REFERENCES `productos` (`id_producto`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
